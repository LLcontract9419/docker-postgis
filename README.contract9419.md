# README.contract9419

## Building the images

```
$ make 10-2.4
``

## Exporting the images

Check `https://hub.docker.com/r/ninech/netbox/tags/` for tags.

```
$ docker image save mdillon/postgis:10-alpine | nice xz -zecv > postgis-10-alpine.image.xz
```

## Bundling files for transfer to other server

```
$ tar cvfz postgis-10-alpine.bundle.tar postgis-10-alpine.image.xz README.contract9419 postgis.env docker-compose.yml
```

## Importing the images

```
$ xz -dvc postgis-10-alpine.image.xz | docker image import - mdillon/postgis:10-alpine
```

## Up/down

This will expose the PostgreSQL/PostGIS instance on port 5432/tcp.

```
$ docker-compose up --no-build --no-start
$ docker-compose up --no-build
CTRL-C
$ docker-compose up --no-build --detach
```

```
$ docker-compose down
```

## Starting/stopping

```
$ docker-compose start
$ docker-compose stop
```

## Connecting to the database

### Connecting with `docker exec`

```
$ docker exec -it docker-postgis_postgis_1 /usr/local/bin/psql -h localhost -U postgres postgres
```

### Connecting on port 5432/tcp

## Database backup

### Backup

```
$ docker exec docker-netbox_postgres_1 pg_dumpall --inserts -U postgres -c -h localhost | grep 54
```

### Restore

```
```

## Git operations on this repository

```
git pull upstream master
git push contract9419 master
```

